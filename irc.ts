import { None, Some, match, Maybe, Authentication } from './types';
import {
  createConnection,
  Socket
} from 'net';

export interface Machine {
  auth:       Maybe<Authentication>,
  port:       Maybe<number>,
  line:       Maybe<string>,
  message:    Maybe<string>,
  connection: Maybe<Socket>,
  error:      Maybe<Function>,
  connected:  boolean,
}

export async function connect(state: Machine): Promise<Machine> {
  const auth = state.auth;
  const port = state.port;
  return match<Authentication>(auth,
    credentials => {
      return state.connection = Some<Socket>(createConnection({
        host: credentials.server, port: match<number>(port,
          _  => _,
          _  => 6667)
      }, () => {
        state.connected = true;
        authenticate(state);
      })), state;
    },
    _           => {
      throw new ReferenceError('No credentials provided');
  });
}

export function authenticate(state: Machine): void {
  match<Authentication>(state.auth,
    credentials => match<Socket>(state.connection,
      connection => {
        connection.write(`USER ${credentials.nick} * * :bot\r\n`);
        connection.write(`NICK ${credentials.nick}\r\n`);
        setTimeout(() => {
          connection.write(`JOIN ${credentials.channel}\r\n`);
        }, 1000);
      },
      _         => {
       throw new ReferenceError('Connection not established');
    }),
    _          => {
      throw new ReferenceError('Not authenticated');
    });
}