import { ƛ, Connect, server, message, reply, on } from './lib/dsl';

const sandbox = require('sandbox');
const virtual = new sandbox();

ƛ({
  server:  'irc.freenode.org',
  nick:    'qj2',
  channel: '##frontend'
  },
  Connect(6667) (
    line => on(/PING (\S+)/) (_ => {
      server`PONG ${_}`
    }),
    line => message(/^\.js (.+)/) (_ => {
      virtual.run(_, out => {
        let safe = out.result.replace(/\r|\n/g, '')
          .replace(/\\'/g, "'")
          .slice(0, 400)
        reply`${safe}`
      })
    }),
    line => message(/^\.version$/) (_ => {
      reply`qj v2.0.0 ( source: https://gist.github.com/jahan-addison/093e87eef768073bc9ead499d088df10 )`
    })
),
  error => '💥 Something went wrong.'
);
