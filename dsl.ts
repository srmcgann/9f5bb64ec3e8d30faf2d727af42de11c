import { None, Some, match, Authentication } from './types';
import * as Irc from './irc';
import { Socket } from 'net';

let State: Irc.Machine = {
  connection: None,
  auth:       Some({
    server:  'irc.freenode.org',
    nick:    'dslbot',
    channel: '##dslbottesting'
  }),
  port:       None,
  message:    None,
  line:       None,
  error:      None,
  connected:  false,
}

function applyMany(context: Irc.Machine, applied: Function[]): void {
  applied.forEach((callback: Function) => {
    match<string>(context.line,
      line => {
        callback.apply(line);
      },
      _    => {
        console.warn('context was insufficient');
      }
    );
  });
}

export function ƛ(details: Authentication, _, Error: Function) {
  State.auth  = Some(details);
  State.error = Some(Error);
}

export function Connect(port: number): Function {
  State.port = Some(port);
  return (...execution) => {
    try {
      Irc.connect(State).then(state => {
        match(State.connection,
          client => {
            client.on('data', data => {
              State.line = Some(data.toString().split(/\r\n/)[0]);
              const message = data.toString().match(/PRIVMSG \S+ :(.+)/);
              if (message) {
                State.message = Some(message[1].trim())
              }
              applyMany(State, execution);
            });
          },
          _      => {
            throw new ReferenceError('Failed to connect');
          }
      );
      });
    } catch(e) {
      match<Function>(State.error,
        call => call(),
        _    => {
          console.error('Uh oh ⚡️', e);
        });
    }
    };
}

export function server(prefix, response): boolean {
  return match<Socket>(State.connection,
    connection => {
      return connection.write(`${prefix[0]} :${response}\r\n`);
    },
    _          => false);
}

export function reply(response, data?): boolean {
  return match<Socket>(State.connection,
    connection => {
    const where = match<Authentication>(State.auth,
      credentials => credentials.channel,
      ()          => '##dslbottesting');
    return connection.write(`PRIVMSG ${where} :${data || response}\r\n`);
  },
    _          => false);
}

export function message(what:RegExp): Function {
  return (callback) => {
    match(State.message,
      line => {
        if (what.test(line)) {
          const capture = what.exec(line);
          callback(capture ? capture[1] : line.split(/:/)[2]);
          State.message = None;
        }
      },
      _    =>  false)
  }
}

export function on(what: RegExp): Function {
  return (callback) => {
    match(State.line,
      line => {
        if (what.test(line)) {
          const capture = what.exec(line);
          callback(capture ? capture[1] : line.split(/:/)[2]);
        }
      },
      _    => {
      throw new ReferenceError('Connection not established');
    })
  }
}
