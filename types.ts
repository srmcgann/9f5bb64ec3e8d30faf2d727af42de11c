export type Maybe<T> = Some<T> | None;
export type None     = () => undefined;
export type Some<T>  = () => T;

export const None: None = () => void(0);
export function Some<T>(_: T): Some<T> {
  return () => _;
}

export function match<T>(_: Maybe<T>, some: (_: any) => any, none: Function) {
  if (Guard<T>(_)) {
    return some(_());
  } else {
    return none();
  }
}
export function Guard<T>(_: Maybe<T>): _ is Maybe<T> {
  return (<Maybe<T>>_)() !== None();
}

export interface Authentication {
  server: string,
  nick: string,
  channel: string
}